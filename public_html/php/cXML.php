<?php
    /**
     * Класс объекта SimpleXML
     * @param $url URL
     */
    class cXML {
        public $_xml;

        public function cXML($url){
            if(file_exists($url)){
                $this->_xml = simplexml_load_file($url);
            }
        }
        
        
        public function ReturnJSONData(){
            $res = json_encode($this->_xml);
            return $res;
        }
    
    }
   

?>