/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function Graph(){
    this.graph = null;
}

Graph.prototype.BuildGraph = function(filename){
    var ajax = $.ajax({
        url: "./php/loadxml.php?url="+filename,
        async: false,
        dataType: 'JSON'
    });
    
    this.graph = JSON.parse(ajax.responseText);
};
